package com.thomas.ribbit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignupActivity extends Activity {

	EditText mUsername, mFirstName, mLastName, mEmail, mPassword;
	Button mRegisterBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		
		mUsername = (EditText) findViewById(R.id.usernameText);
		mFirstName = (EditText) findViewById(R.id.firstNameText);
		mLastName = (EditText) findViewById(R.id.lastNameText);
		mEmail = (EditText) findViewById(R.id.emailText);
		mPassword = (EditText) findViewById(R.id.passwordText);
		mRegisterBtn = (Button) findViewById(R.id.registerButton);
		
		mRegisterBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String username = mUsername.getText().toString().trim();
				String fname = mFirstName.getText().toString().trim();
				String lname = mLastName.getText().toString().trim();
				String email = mEmail.getText().toString().trim();
				String password = mPassword.getText().toString().trim();
				
				if(
						username.isEmpty() ||
						fname.isEmpty() || 
						lname.isEmpty()|| 
						email.isEmpty() || 
						password.isEmpty()){
					AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
					builder.setMessage(R.string.signup_error_message)
					.setTitle(R.string.signup_error_title)
					.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				}else{
					ParseUser user = new ParseUser();
					user.setEmail(email);
					user.setPassword(password);
					user.setUsername(username);
					user.put("firstName",fname);
					user.put("lastName", lname);
					user.signUpInBackground(new SignUpCallback(){

						@Override
						public void done(ParseException e) {
							// TODO Auto-generated method stub
							if(e == null){
								//success
								Intent intent = new Intent(SignupActivity.this,MainActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
								startActivity(intent);
							}else{
								AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
								builder.setMessage(e.getMessage())
								.setTitle(R.string.signup_error_title)
								.setPositiveButton(android.R.string.ok, null);
								AlertDialog dialog = builder.create();
								dialog.show();
								
							}
							
						}});
				}
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signup, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
